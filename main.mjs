import {utils} from './utils.mjs'
import ini from 'ini';
import fs from 'fs';
import firebase from 'firebase';
import uuid from 'uuid';
import Firebird from 'node-firebird';

var config = ini.parse(fs.readFileSync('config.ini', 'utf-8'));
var  options = { host : config.firebird.dataBaseServer
                , port : config.firebird.port
                , database : config.firebird.dataBaseAddress
                , user : config.firebird.user
                , password : config.firebird.password
                , lowercase_keys : config.firebird.lowercase_keys
                , role : config.firebird.role
                , pageSize : config.firebird.pagesize };
                
var firebaseconfig = {  apiKey : config.firebase.apikey
                        , authDomain : config.firebase.authdomain
                        , databaseURL : config.firebase.databaseurl };
var user_id = config.set.user_id;
var libera_registros = config.set.libera_registros;

var db = initFirebase(firebaseconfig);
var dbUsers =  db.child("users");
var dblogins =  db.child("logins");

let times = config.set.times;
let time =  86400000/times;

let cnpj;
let cnpjNaoEncotrado;
dbUsers.child(user_id).child("cnpj").on("value", cnpj => {
    cnpj = cnpj.val();

    Firebird.attach(options, (err, db) => {
        if (err) {  throw err; }
        
        let sql =  `select CGC from parametros where CGC = ${cnpj}`;
        cnpjNaoEncotrado = true;
        db.sequentially(sql,null,(row,index) => {
           liberarAcesso();
        }, (err) => {
            if (cnpjNaoEncotrado == true) {
                console.log("Chave não encontrada, favor verificar!!")
            }
               if (err) {
                console.log("Select retornou erro!");
            }
        });
    });
});



function liberarAcesso() {
    setInterval(()=>main(),time);
    main();
}



function main (){

    let dbsummary = dbUsers.child(user_id).child("summary");
    let dblancamentos = dbUsers.child(user_id).child("lancamentos");
    let dbfiltros = dbUsers.child(user_id).child("filtros");
    let dbfiliais = dbUsers.child(user_id).child("filiais");
    let dbhomeSummary = dbUsers.child(user_id).child("home_summary");
    let dblastmonths = dbUsers.child(user_id).child("last_months");
   

    getLastDate(data =>{


        dblancamentos.remove(l => console.log("Documentos de lancamentos liberados!" ));
        dbfiltros.remove(f => console.log("Documentos de filtros liberados!" ));
        dbfiliais.remove(f => console.log("Documentos de filiais liberados!" ));
        dbsummary.remove(f => console.log("Documentos de summary liberados!" ));
        dbhomeSummary.remove(f => console.log("Documentos de home summary liberados!" ));
        dblastmonths.remove(f => console.log("Documentos dos ultimos meses liberados"));


        let lastMonths =  getLastMonths(data);
        dbUsers.child(user_id).child("last_months").set(lastMonths);
        console.log("Last Months importado com sucesso!");

        getVisaoGeral(lastMonths, (row,index)=> {
                let quantidade_summary  = row["TOTAL_QUANTIDADE"];
                quantidade_summary = quantidade_summary.round(1);
                let valor_summary  = row["TOTAL_VALOR"];
                valor_summary = valor_summary.round(2);
                let comissao_summary  = row["TOTAL_COMISSAO"];
                if (comissao_summary) {
                    comissao_summary = comissao_summary.round(2);
                }
                let custo_summary  = row["TOTAL_CUSTO_UNITARIO"];
                custo_summary = custo_summary.round(2);
                let mes = row["MES"];
                let cd_filial = row["CD_FILIAL"];
                let cnpj = readEasier(row["FILIAL_CNPJ"]);
                let tipo_venda = readEasier(row["TIPO_VENDA"]);
                let tipo_venda_descrito = readTipoVenda(tipo_venda);
                let filial_razao_social =  readEasier(row["FILIAL"]);
              
                dbsummary.child(mes)
                        .child(cd_filial)
                        .child(tipo_venda) 
                .set({cd_filial :cd_filial, mes : mes, cnpj : cnpj, tipo_venda_descrito : tipo_venda_descrito
                        , tipo_venda : tipo_venda, quantidade : quantidade_summary, valor : valor_summary
                        , comissao : comissao_summary, custo_unitario : custo_summary, filial_razao_social :filial_razao_social});
        }, ()=>{
            console.log("Visão Geral importado com sucesso!");
            getHomeSummary(`'T','V','C','B','D','F'`,(row, ix) =>{
                 let valor = row["VALOR"].round(2);
                 let tipo_venda = readEasier(row["TIPO"]);
         
                 dbUsers.child(user_id).child("home_summary").child(tipo_venda).set({
                     tipo_lancamento : readTipoVenda(tipo_venda),
                     tipo_venda : tipo_venda,
                     valor : valor});
                             
                 console.log(tipo_venda,valor);
                 },() => {
                     console.log("Home summary importado com sucesso!");
                     getLancamentos(lastMonths);
             });    
        });

    })



};


function getVisaoGeral(lastMonths,rb, cb) {
    Firebird.attach(options, (err, db) => {
        if (err) {  throw err; }
        let sql =  `select l.CD_FILIAL, fil.CNPJ as FILIAL_CNPJ, fil.RAZAO as FILIAL
              , EXTRACT (MONTH FROM l.DATA_LANCAMENTO) as MES, l.TIPO_VENDA
              , SUM(l.QUANTIDADE) as TOTAL_QUANTIDADE
              , SUM(l.VALOR) as TOTAL_VALOR
              , SUM(l.COMISSAO) as TOTAL_COMISSAO
              , SUM(l.CUSTO_UNITARIO) as TOTAL_CUSTO_UNITARIO
        from  LANCAMENTOS as l
        inner join FILIAIS as fil on ( l.CD_FILIAL = fil.CD_FILIAL )
        where l.DATA_LANCAMENTO >= '${lastMonths.month4.code}/01/${lastMonths.month4.year}' 
        group by l.CD_FILIAL, fil.CNPJ, fil.RAZAO
        , EXTRACT (MONTH FROM l.DATA_LANCAMENTO), l.TIPO_VENDA`;

        db.sequentially(sql,null,(row,index) => {
            rb(row,index);
        }, (err) => {
            db.detach();
            cb();
        });
    });
}



function getLastDate( cb) {
    let data;
    Firebird.attach(options, (err, db) => {
        if (err) {  throw err; }
        let sql = `select first 1 data_lancamento from LANCAMENTOS order by data_lancamento desc`;
        db.sequentially(sql,null, (row,index) => {
            data = row["DATA_LANCAMENTO"];
        }, (err) => {
            db.detach();
            cb(data);
        });
    });
}

function getHomeSummary(tipo_venda_in, rb,cb) {   
    Firebird.attach(options, (err, db) => {
        if (err) {  throw err; }
        let sql = `select sum(valor) as valor 
                    , tipo_venda as tipo    
                from lancamentos 
                where tipo_venda in (${tipo_venda_in})
                and data_lancamento = ( select first 1 data_lancamento from LANCAMENTOS order by data_lancamento desc )
                group  by tipo_venda`;
                
        db.sequentially(sql,null,(row,index) => {
            rb(row,index);
        }, (err) => {
            db.detach();
            cb();
        });
    });

}

function getLancamentos( lastMonths ) {
    Firebird.attach(options, (err, db) => {
        if (err) {  throw err; }
     
        let sql =  `select l.CD_LANCAMENTO, l.ID_PRODUTO
            , l.CODIGO_BARRAS, l.DESCRICAO as PRODUTO, l.CD_FILIAL, fil.CNPJ as FILIAL_CNPJ, fil.RAZAO as FILIAL
            , lab.CD_LABORATORIO, lab.CNPJ as LABORATORIO_CNPJ,  lab.RAZAO as LABORATORIO, l.CD_GRUPO
            , gr.DESCRICAO as GRUPO, cv.CD_CONVENIO, cv.NOME as CONVENIO, cv.CNPJ as CONVENIO_CNPJ, l.CD_CLASSE
            , cl.DESCRICAO as CLASSE, l.CD_FORMA_PAGAMENTO as TIPO_PAGAMENTO, l.TIPO_VENDA, l.CUSTO_MEDIO
            , l.CUSTO_UNITARIO, l.DATA_LANCAMENTO, l.DATA_CAIXA, l.QUANTIDADE, l.VALOR, l.CST, l.ICMS, l.ECF, l.CEST
            , l.NOTA_FISCAL, l.ENTREGA, l.HORA, l.NCM, l.CFOP, l.CUSTO_MEDIO * l.QUANTIDADE AS CUSTO_UNITARIO_CALCULADO
            , l.CD_VENDEDOR, v.NOME as VENDEDOR, l.COMISSAO, (l.VALOR - l.ARREDONDAMENTO) AS VALOR_CALCULADO 
        from  LANCAMENTOS as l
        left  outer join FILIAIS as fil on ( l.CD_FILIAL = fil.CD_FILIAL )
        left  outer join LABORATORIOS as lab on ( l.CD_LABORATORIO = lab.CD_LABORATORIO )
        left  outer join GRUPOS as gr  on ( l.CD_GRUPO = gr.CD_GRUPO )
        left  outer join CONVENIOS as cv on ( l.CD_CONVENIO = cv.CD_CONVENIO )
        left  outer join CLASSES as cl on (l.CD_CLASSE = cl.CD_CLASSE )
        left  outer join VENDEDORES as v on (l.CD_VENDEDOR = v.CD_VENDEDOR )
        where l.DATA_LANCAMENTO >= '${lastMonths.month4.code}/01/${lastMonths.month4.year}'
        order by DATA_LANCAMENTO DESC`

        db.sequentially(sql,null,(row,index) => { // TO GIRANDO TODOS OS LANCAMENTOS AQUI DENTRO
        
            console.log(`read lancamento: ${row["CD_LANCAMENTO"]} Firebird Base Local Filial: ${row["CD_FILIAL"]}`);
            
            let lancamento_id = pushLancamentoData(row,index);
            
            console.log(`firebase save ${lancamento_id}` );

        },
        (err) => {
            db.detach();
            dbUsers.child(user_id).child("logins_id").on("value", c => {
              let key = c.val();
              console.log(key);
              dblogins.child(key).child("instalado").set(true);                
            });

            dbUsers.child(user_id).child("instalado").set(true);


            console.log("Cliente instalado com sucesso");  
        });      
    });
}

function pushLancamentoData(row,index){

    // START FILLING VARIABLES

    let cd_lancamento = row["CD_LANCAMENTO"];
    let filial_code = row["CD_FILIAL"];
    let filial_cnpj = readEasier(row["FILIAL_CNPJ"]);
    let filial_razao_social = readEasier(row["FILIAL"]);
    let laboratorio_code = row["CD_LABORATORIO"];
    let laboratorio_cnpj = readEasier(row["LABORATORIO_CNPJ"]);
    let laboratorio_razao_social = readEasier(row["LABORATORIO"]);
    let grupo_code = row["CD_GRUPO"];
    let grupo_razao_social = readEasier(row["GRUPO"]);
    let convenio_code = row["CD_CONVENIO"];
    let convenio_razao_social = row["CONVENIO"];
    let convenio_cnpj = readEasier(row["CONVENIO_CNPJ"]);
    let classe_code = row["CD_CLASSE"];
    let classe_name = readEasier(row["CLASSE"]);
    let tipo_venda_code = readEasier(row["TIPO_VENDA"]);
    let tipo_venda_name = readTipoVenda(readEasier(row["TIPO_VENDA"]));
    let tipo_pagamento_code = row["TIPO_PAGAMENTO"];
    let __vendedor = getVendedor(
                row["CD_VENDEDOR"]
                , readEasier(row["VENDEDOR"])
                , readEasier(row["COMISSAO"]));     
    let __tipo_pagamento = getTipoPagamento(tipo_pagamento_code);

    let dt_lancamento = row["DATA_LANCAMENTO"];
    let dt_caixa = row["DATA_CAIXA"];
    let hora = row["HORA"];
    let dt_lancamento_number = Date.parse(dt_lancamento);
    let dt_caixa_number = Date.parse(dt_lancamento);
    let dt_now_number = Date.parse(new Date());
    let valor = row["VALOR"];

    let lancamento_id = uuid(); 
     let document = {
        id:lancamento_id
        , cd_lancamento: cd_lancamento
        , id_produto: row["ID_PRODUTO"]
        , codigo_barras: readEasier(row["CODIGO_BARRAS"])
        , produto: readEasier(row["PRODUTO"])

        , custo_medio: row["CUSTO_MEDIO"]
        , custo_unitario: row["CUSTO_UNITARIO"]
        , data_lancamento: dt_lancamento.toLocaleString()
        , dt_lancamento_number : dt_lancamento_number
        , data_caixa: dt_caixa.toLocaleString()
        , quantidade: row["QUANTIDADE"]
        , valor: valor
        , cst: readEasier(row["CST"])
        , icms: readEasier(row["ICMS"])
        , ecf: readEasier(row["ECF"])
        , cest: readEasier(row["CEST"])
        , nota_fiscal: readEasier(row["NOTA_FISCAL"])
        , entrega: readEasier(row["ENTREGA"])
        , hora: hora.toTimeString()
        , ncm: readEasier(row["NCM"])
        , cfop: readEasier(row["CFOP"])
        , custo_unitario_calculado:row["CUSTO_UNITARIO_CALCULADO"]
        , valor_calculado : row["VALOR_CALCULADO"]
        , tipo_venda_code : tipo_venda_code
        , filial_code : filial_code
        , laboratorio_code : laboratorio_code
        , grupo_code : grupo_code
        , convenio_code : convenio_code
        , classe_code : classe_code
        , tipo_pagamento_code : tipo_pagamento_code
        , vendedor_code : row["CD_VENDEDOR"]
        , filial: { code: filial_code, razao_social: filial_razao_social
                    , cnpj: filial_cnpj }
        , laboratorio: { code: laboratorio_code, razao_social: laboratorio_razao_social 
                        , cnpj: laboratorio_cnpj}
        , grupo: {  code: grupo_code
                    , grupo: grupo_razao_social }
        , convenio: { code: convenio_code, razao_social: convenio_razao_social, cnpj: convenio_cnpj }
        , classe: {  code: classe_code , classe: classe_name }             
        , tipo_pagamento: __tipo_pagamento
        , tipo_venda: { code: tipo_venda_code , name: tipo_venda_name }
        , vendedor: __vendedor 
        };

    let dblancamentos = dbUsers.child(user_id).child("lancamentos");
    let dbfiltros = dbUsers.child(user_id).child("filtros");
    let dbfiliais = dbUsers.child(user_id).child("filiais");
    let dbsummary = dbUsers.child(user_id).child("summary");
    let tipo_lancamento = readTipoLancamento(tipo_venda_code);


    if (valor) {
         valor = valor.round(2)
    }

    dbfiltros.child("base").child("todos").child(dt_lancamento_number).set({dt_lancamento_number : dt_lancamento_number});
    dbfiltros.child("base").child("todos").child(dt_lancamento_number).child(filial_cnpj).set({ filial_cnpj: filial_cnpj});
    dbfiltros.child("details").child("todos").child(dt_lancamento_number).child(filial_cnpj).child(lancamento_id).set({ hora: hora, valor:valor, tipo_venda_descrito: readTipoVenda(tipo_venda_code)});


    dbfiltros.child("base").child(tipo_lancamento).child(dt_lancamento_number).set({dt_lancamento_number : dt_lancamento_number});
    dbfiltros.child("base").child(tipo_lancamento).child(dt_lancamento_number).child(filial_cnpj).set({ filial_cnpj: filial_cnpj});
    dbfiltros.child("details").child(tipo_lancamento).child(dt_lancamento_number).child(filial_cnpj).child(lancamento_id).set({ hora: hora, valor:valor, tipo_venda_descrito: readTipoVenda(tipo_venda_code)});



    // FINISHING FILLING VARIABLES

    // dbfiltros.child("base").child(dt_lancamento_number).set({dt_lancamento_number : dt_lancamento_number
    //     , tipo_venda : tipo_lancamento, grupo : grupo_code, classe : classe_code})
        

    // dbfiltros.child("base").child(dt_lancamento_number).set({dt_lancamento_number : dt_lancamento_number
    //         , tipo_venda : tipo_lancamento, grupo : grupo_code, classe : "todos"})
        

    // dbfiltros.child("base").child(dt_lancamento_number).set({dt_lancamento_number : dt_lancamento_number
    //     , tipo_venda : tipo_lancamento, grupo : "todos", classe : "todos"})
    

    // dbfiltros.child("base").child(dt_lancamento_number).set({dt_lancamento_number : dt_lancamento_number
    //         , tipo_venda : tipo_lancamento, grupo : "todos", classe : classe_code})


    // dbfiltros.child("base").child(dt_lancamento_number).set({dt_lancamento_number : dt_lancamento_number
    //     , tipo_venda : "todos", grupo : grupo_code, classe : "todos"})


    // dbfiltros.child("base").child(dt_lancamento_number).set({dt_lancamento_number : dt_lancamento_number
    //     , tipo_venda : "todos", grupo : grupo_code, classe : classe_code})

    
    // dbfiltros.child("base").child(dt_lancamento_number).set({dt_lancamento_number : dt_lancamento_number
    //         , tipo_venda : "todos", grupo : "todos", classe : classe_code})

    
    // dbfiltros.child("base").child(dt_lancamento_number).set({dt_lancamento_number : dt_lancamento_number
    //             , tipo_venda : "todos", grupo : "todos", classe : "todos"})
    
        
    // dbfiltros.child("base").child(dt_lancamento_number).child(filial_cnpj).set({filial_cnpj:filial_cnpj})

    // if (!grupo_code) {  grupo_code = "todos"; }


    // if (!classe_code) { classe_code = "todos"; }


    // dbfiltros.child("details").child(dt_lancamento_number).child(filial_cnpj).child(`${tipo_lancamento}_${grupo_code}_${classe_code}`).set(lancamento_id)//.set({ hora: hora, valor:valor, tipo_venda_descrito: readTipoVenda(tipo_venda_code)});
    // dbfiltros.child("details").child(dt_lancamento_number).child(filial_cnpj).child(`${tipo_lancamento}_${grupo_code}_todos`).set(lancamento_id)//.set({ hora: hora, valor:valor, tipo_venda_descrito: readTipoVenda(tipo_venda_code)});
    // dbfiltros.child("details").child(dt_lancamento_number).child(filial_cnpj).child(`${tipo_lancamento}_todos_todos`).set(lancamento_id)//.set({ hora: hora, valor:valor, tipo_venda_descrito: readTipoVenda(tipo_venda_code)});
    // dbfiltros.child("details").child(dt_lancamento_number).child(filial_cnpj).child(`${tipo_lancamento}_todos_${classe_code}`).set(lancamento_id)//.set({ hora: hora, valor:valor, tipo_venda_descrito: readTipoVenda(tipo_venda_code)});
    // dbfiltros.child("details").child(dt_lancamento_number).child(filial_cnpj).child(`todos_${grupo_code}_todos`).set(lancamento_id)//.set({ hora: hora, valor:valor, tipo_venda_descrito: readTipoVenda(tipo_venda_code)});
    // dbfiltros.child("details").child(dt_lancamento_number).child(filial_cnpj).child(`todos_${grupo_code}_${classe_code}`).set(lancamento_id)//.set({ hora: hora, valor:valor, tipo_venda_descrito: readTipoVenda(tipo_venda_code)});
    // dbfiltros.child("details").child(dt_lancamento_number).child(filial_cnpj).child(`todos__todos_${classe_code}`).set(lancamento_id)//.set({ hora: hora, valor:valor, tipo_venda_descrito: readTipoVenda(tipo_venda_code)});
    // dbfiltros.child("details").child(dt_lancamento_number).child(filial_cnpj).child(`todos_todos_todos`).set(lancamento_id)//.set({ hora: hora, valor:valor, tipo_venda_descrito: readTipoVenda(tipo_venda_code)});



    // WORK LANCAMENTOS
    dblancamentos.child(lancamento_id).set(document);

     return lancamento_id;
}




function getTipoPagamento(code){
    console.log(code);
    if (code) {
        console.log("Tipo de pagamento definido")
        return{ code: code, name: readTipoPagamento(code) }
    } else { return {code: code, name: "-Dinheiro"} } 
}

function getVendedor(code,name,comissao) {
    if ((comissao) > 0) {
        console.log("Lancamento comissionado")
        return { code: code, name : name, comissao : comissao }
    } else return {}
}

function readEasier(str){
    return utils.Capitalizer(utils.readBuffer(str));
}


function readTipoVenda(code) {
    if ( code ) {
        if ( code == 'B' ) return "Balanço"
        else if ( code == 'C' ) return "Compra"
        else if ( code == 'D' ) return "Devolução"
        else if ( code == 'E' ) return "Excluido" 
        else if ( code == 'F' ) return "Nota de Bonificação"
        else if ( code == 'M' ) return "Compra de Emergência"
        else if ( code == 'N' ) return "NFE Devolução"
        else if ( code == 'S' ) return "Sangria"
        else if ( code == 'T' ) return "Transferência"
        else if ( code == 'V' ) return "Venda"
        else if ( code == 'X' ) return "Acerto de Estoque"
        else if ( code == 'U' ) return "Suprimento"
        else return `Tipo do Movimento é ${code}`
    }
  }

function readTipoLancamento(code) {
    if ( code ) {
        if ( code == 'B' ) return "balancos"
        else if ( code == 'C' ) return "compras"
        else if ( code == 'D' ) return "devolucoes"
        else if ( code == 'E' ) return "excluidos" 
        else if ( code == 'F' ) return "notas-bonificacoes"
        else if ( code == 'M' ) return "compras-emergencias"
        else if ( code == 'N' ) return "nfe-devolucoes"
        else if ( code == 'S' ) return "sangrias"
        else if ( code == 'T' ) return "transferencias"
        else if ( code == 'V' ) return "vendas"
        else if ( code == 'X' ) return "acertos-estoques"
        else if ( code == 'U' ) return "suprimentos"
        else return null
    }
}


function readTipoPagamento(code) {
    if ( code ) {
        if ( code == 1 ) return "Dinheiro"
        else if ( code == 2 ) return "Cartão de Crédito"
        else if ( code == 3 ) return "Convênio"
        else if ( code == 4 ) return "Cheque Predatado" 
        else if ( code == 5 ) return "Crédito"
        else if ( code == 6 ) return "Cheque a Vista"
        else if ( code == 100 ) return "Devolção"
        else if ( code == 101 ) return "Transferência"
        else return `Tipo de pagamento desconhecido, é ${code}`;
    }
}

function initFirebase(config) {
    firebase.initializeApp(config);
    return firebase.database().ref()
}
 

Number.prototype.round = function(precision) {
    var factor = Math.pow(10, precision);
    return Math.round(this * factor) / factor;
};

function getLastMonths(date) {
    
    var arrayMes = new Array();
    arrayMes[0] = "JAN";
    arrayMes[1] = "FEV";
    arrayMes[2] = "MAR";      
    arrayMes[3] = "ABR";
    arrayMes[4] = "MAI";
    arrayMes[5] = "JUN";
    arrayMes[6] = "JUL";
    arrayMes[7] = "AGO";
    arrayMes[8] = "SET";
    arrayMes[9] = "OUT";
    arrayMes[10] = "NOV";
    arrayMes[11] = "DEZ";
    
    let year = date.getFullYear();
    let nextMonth = date.getMonth();

    let numerMonth1 = nextMonth;
    let year1 = year;
    let month1 = arrayMes[nextMonth];
    nextMonth--;
    
    if (nextMonth == -1) { nextMonth = 11; year--  } 
    let numerMonth2 = nextMonth;
    let year2 = year;
    let month2 = arrayMes[nextMonth];
    nextMonth--;
    
    if (nextMonth == -1) { nextMonth = 11; year-- } 
    let numerMonth3 = nextMonth;
    let year3 = year;
    let month3 = arrayMes[nextMonth];
    nextMonth--;
    
    if (nextMonth == -1) { nextMonth = 11; year-- } 
    let numerMonth4 = nextMonth;
    let year4 = year;
    let month4 = arrayMes[nextMonth];
    

    return {month1 : { code: numerMonth1+1, text:month1, year:year1}
            , month2 :{ code: numerMonth2+1, text:month2, year:year2}
            , month3 : { code: numerMonth3+1, text:month3, year:year3}
            , month4 : { code: numerMonth4+1, text:month4, year:year4} }
}


function GetDateAsStringMMddYYYY (data ) {

    var dd = data.getDay();
    var mm = data.getMonth()+1; //January is 0!
    var yyyy = data.getFullYear();
    if(dd<10){
        dd ='0'+dd;
    } 
    if(mm<10){
        mm ='0'+mm;
    } 
    return dd+'/'+mm+'/'+yyyy;

};




