import {utils} from './utils.mjs'
import ini from 'ini';
import fs from 'fs';
import firebase from 'firebase';
import uuid from 'uuid';
import Firebird from 'node-firebird';

var config = ini.parse(fs.readFileSync('config.ini', 'utf-8'));
var  options = { host : config.firebird.dataBaseServer, port : config.firebird.port, database : config.firebird.dataBaseAddress, user : config.firebird.user
    , password : config.firebird.password, lowercase_keys : config.firebird.lowercase_keys, role : config.firebird.role, pageSize : config.firebird.pagesize };  
var firebaseconfig = {apiKey:config.firebase.apikey, authDomain:config.firebase.authdomain,databaseURL:config.firebase.databaseurl};

options.database = "D:\\Bancos\\SUPORTE.FDB";

firebase.initializeApp(firebaseconfig);
let dbRootRef = firebase.database().ref("users");

//reset();

//getUpdates();

//loadGeral();



{/*option value="2">Option 2</option> */}


function snapshotToArray(snapshot) {
    var returnArr = [];
  
    snapshot.forEach(function(childSnapshot) {
        var item = childSnapshot.val();
        item.key = childSnapshot.key;
  
        returnArr.push(item);
    });
  
    return returnArr;
};



function getUpdates(){
    dbRootRef.on("value", (snapshot) => {
        snapshot.forEach(data =>{
            processSolicitacao(data.key, key =>{
                dbRootRef.child(key).on("value",(snap)=>{
                    let c = snap.val();
                    if ( c.solicitado == true) {
                        fazerChamado(c.internal_code);
                    }
                });
            });
        });
    });
}


function processSolicitacao(key,cb){
        dbRootRef.child(key).on("child_changed",(snap) =>{
            if (snap.key=="solicitado") {
                if ((snap.val()) == true) {
                    let key = snap.ref.parent.key;
                    cb(key);
                }
            }
        });
};


function fazerChamado(code){
    
    Firebird.attach(options, (err, db) => {
        if (err) {  
            console.log("Erro de conexão com o Firebird"); 
            return;
        }
            let r;
            let sql = `select c.ID_CLIENTE, c.RAZAO_SOCIAL, C.CNPJ from CLIENTES as c where c.ID_CLIENTE = ${code}`
            db.sequentially(sql,null,(row,index) => {
                let r = row;
            }, (err) => {
                console.log("Passou aqui");
                console.log(r);


                db.detach();
            });
    });
}

function reset() {
    deleteGrupos();
    deleteClasses();
    deleteTipoPagamentos();
    deleteClientes();
    
    setGrupos();
    setClasses();
    setTipoPagamentos();
    setClientes();


    //tem que deletar esse aqui
    //57542146-a7e3-4893-8654-67d9d1501d11
}


function deleteTipoPagamentos(){
    let dbtipoPagamentos = firebase.database().ref("tipo_pagamentos");
    dbtipoPagamentos.remove(c =>{
        if (c==null) { 
            console.log("Todos os Tipos de Pagamentos excluidas com sucesso!"); 
        }
    }); 
}



function deleteGrupos(){
    let dbgrupos = firebase.database().ref("grupos");
    dbgrupos.remove(c =>{
        if (c==null) { 
            console.log("Todos os Grupos excluidos com sucesso!"); 
        }
    }); 
}

function deleteClasses(){
    let dbclasses = firebase.database().ref("classes");
    dbclasses.remove(c =>{
        if (c==null) { 
            console.log("Todas as Classes excluidas com sucesso!"); 
        }
    }); 
}


function deleteClientes(){
    dbRootRef.remove(c =>{
        if (c==null) { 
            console.log("Todos os Clientes excluidos com sucesso!"); 
        }
    }); 
}

function setClientes(){

    Firebird.attach(options, (err, db) => {
        if (err) {  
            console.log("Erro de conexão com o Firebird"); 
            return;
        }

        let sql = `select c.ID_CLIENTE, c.RAZAO_SOCIAL, C.CNPJ from CLIENTES as c where c.status = 'A'`
        db.sequentially(sql,null,(row,index) => {

            let cnpj = utils.readBuffer(row["CNPJ"]);
            let razao = utils.Capitalizer(utils.readBuffer(row["RAZAO_SOCIAL"]));
            let internal_code = row["ID_CLIENTE"];
            let autorizado = false;
            let instalado = false;
            let solicitado = false;
            let id = uuid();
            console.log(index);
            console.log(`read cliente: ${internal_code} Firebird Base Local CNPJ: ${cnpj}` );
            let dbClients = firebase.database().ref("clients");
            dbRootRef.child(id).set({id:id,cnpj:cnpj,razao:razao,internal_code:internal_code,autorizado:autorizado,solicitado:solicitado,instalado:instalado})
            dbClients.child(cnpj).set({id:id});
            console.log(`firebase save ${id}` );
        },
        (err)=>{
            db.detach();
            console.log("\n");
            console.log("🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-");
            console.log("\n");
            console.log("---------Clientes instalados com sucesso--------");
            console.log("\n");
            console.log("🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-🔥-");
        });
    });

}

function setClasses(){

    let dbclasses = firebase.database().ref("classes");
    dbclasses.child(1).set({code:1,name:'Utilidades', type:'OU'});
    dbclasses.child(4).set({code:4,name:'Naturais', type:'OU'});
    dbclasses.child(5).set({code:5,name:'Medicamento', type:'MD'});
    dbclasses.child(6).set({code:6,name:'Perfumaria', type:'OU'});
    dbclasses.child(7).set({code:7,name:'Alimentos e Bamboniere', type:'OU'});
    dbclasses.child(8).set({code:8,name:'Oficinais e Hospitalares', type:'OU'});
    dbclasses.child(10).set({code:10,name:'Medicamento Generico', type:'MD'});
    dbclasses.child(11).set({code:11,name:'Medicamentos Bonificado', type:'MD'});
    dbclasses.child(12).set({code:12,name:'Medicamento NC', type:'MD'});


    console.log("Todos as Classes cadastras com sucesso!"); 
}

function setTipoPagamentos(){

    let dbtipoPagamentos = firebase.database().ref("tipo_pagamentos");

    dbtipoPagamentos.child(1).set({code:1,name:'Dinheiro'});
    dbtipoPagamentos.child(2).set({code:2,name:'Cartão de Crédito'});
    dbtipoPagamentos.child(3).set({code:3,name:'Convênio'});
    dbtipoPagamentos.child(4).set({code:4,name:'Cheque Predatado'});
    dbtipoPagamentos.child(5).set({code:5,name:'Crédito'});
    dbtipoPagamentos.child(6).set({code:6,name:'Cheque a Vista'});
    dbtipoPagamentos.child(100).set({code:100,name:'Devolução'});
    dbtipoPagamentos.child(101).set({code:101,name:'Transferência'});


    console.log("Todos os Tipos de Pagamento cadastros com sucesso!"); 


}

function setGrupos() {

let dbgrupos = firebase.database().ref("grupos");

    dbgrupos.child(2).set({code:2,name:'Naturais'});
    dbgrupos.child(5).set({code:5,name:'Ético'});
    dbgrupos.child(8).set({code:8,name:'Controlado'});
    dbgrupos.child(19).set({code:19,name:'Liberado'});

    dbgrupos.child(28).set({code:28,name:'Genérico'});
    dbgrupos.child(30).set({code:30,name:'Sem Uso'});
    dbgrupos.child(33).set({code:33,name:'Oficinais'});
    dbgrupos.child(40).set({code:40,name:'Bonificado'});
    dbgrupos.child(42).set({code:42,name:'Hidrante Óleo'});

    dbgrupos.child(43).set({code:43,name:'Tinturas'});
    dbgrupos.child(45).set({code:45,name:'Doces'});
    dbgrupos.child(46).set({code:46,name:'Hospitalar'});
    dbgrupos.child(47).set({code:47,name:'Infantil'});
    dbgrupos.child(49).set({code:49,name:'Maquiagem'});
    dbgrupos.child(55).set({code:55,name:'Mamadeiras Chupetas'});
    dbgrupos.child(59).set({code:59,name:'Sabonetes'});


    dbgrupos.child(61).set({code:61,name:'Esmalte Adj'});
    dbgrupos.child(62).set({code:62,name:'Repelente'});
    dbgrupos.child(66).set({code:66,name:'Desodorante'});
    dbgrupos.child(68).set({code:68,name:'Aboservente Fem.'});
    dbgrupos.child(69).set({code:69,name:'Fralda Inf'});
    dbgrupos.child(70).set({code:70,name:'Depilatorio'});
    dbgrupos.child(74).set({code:74,name:'Talcos'});
    dbgrupos.child(77).set({code:77,name:'Barba'});
    dbgrupos.child(78).set({code:78,name:'Eticos Var.'});
    dbgrupos.child(79).set({code:79,name:'Bonif. Var.'});
    dbgrupos.child(80).set({code:80,name:'Gener. Var.'});
    dbgrupos.child(81).set({code:81,name:'Alimentos'});
    dbgrupos.child(82).set({code:82,name:'Protetor / Solar'});



    dbgrupos.child(85).set({code:85,name:'Encalho B.'});
    dbgrupos.child(86).set({code:86,name:'Tranforma.'});
    dbgrupos.child(87).set({code:87,name:'Sandalias'});
    dbgrupos.child(88).set({code:88,name:'Escova / Pente.'});
    dbgrupos.child(89).set({code:89,name:'Preservativo'});
    dbgrupos.child(90).set({code:90,name:'Fixadores'});
    dbgrupos.child(91).set({code:91,name:'Pilhas'});
    dbgrupos.child(92).set({code:92,name:'Colonia'});
    dbgrupos.child(94).set({code:94,name:'Acesso. Cab'});
    dbgrupos.child(95).set({code:95,name:'Papel Higienico'});
    dbgrupos.child(96).set({code:96,name:'Trat. Cap.'});
    dbgrupos.child(97).set({code:97,name:'Inset/ Rep'});
    dbgrupos.child(99).set({code:99,name:'Lenço Umidecido'});



    dbgrupos.child(100).set({code:100,name:'Creme Dental'});
    dbgrupos.child(101).set({code:101,name:'Sol. Bucal'});
    dbgrupos.child(102).set({code:102,name:'Escova Dental'});
    dbgrupos.child(103).set({code:103,name:'Fio Dental'});
    dbgrupos.child(106).set({code:106,name:'Gener. Cont.'});
    dbgrupos.child(107).set({code:107,name:'Shampoo'});
    dbgrupos.child(108).set({code:108,name:'Condicionador'});
    dbgrupos.child(109).set({code:109,name:'Creme Trat.'});
    dbgrupos.child(110).set({code:110,name:'Creme de Pentear'});
    dbgrupos.child(111).set({code:111,name:'Encomenda'});
    dbgrupos.child(114).set({code:114,name:'Diversos'});

    dbgrupos.child(115).set({code:115,name:'Descoloran.'});
    dbgrupos.child(122).set({code:122,name:'Anasol'});
    dbgrupos.child(126).set({code:126,name:'Inutilizado'});
    dbgrupos.child(130).set({code:130,name:'Inutilizado 2'});
    dbgrupos.child(131).set({code:131,name:'Etico Anti'});

    dbgrupos.child(133).set({code:133,name:'Gener Anti'});
    dbgrupos.child(133).set({code:134,name:'Boni Antib'});
    dbgrupos.child(135).set({code:135,name:'Gen S/ Comissão'});
    dbgrupos.child(136).set({code:136,name:'FP Comissão'});
    dbgrupos.child(137).set({code:137,name:'FP S/ Comissão'});


    dbgrupos.child(142).set({code:142,name:'Anticoncepcionais'});
    dbgrupos.child(143).set({code:143,name:'I2GO Acessórios Cel'});
    dbgrupos.child(144).set({code:144,name:'Esc. Dental Dentil.'});
    dbgrupos.child(145).set({code:145,name:'Inutilizado 1'});
    dbgrupos.child(146).set({code:146,name:'Inutilizado 2'});
    dbgrupos.child(147).set({code:147,name:'Verde Brasil'});



    console.log("Todos os Grupos cadastros com sucesso!"); 




}
