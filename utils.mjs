'use strict';

const utils = {
                Capitalizer(value) {
                    return  value.toLowerCase().replace(/\b\w/g, l => l.toUpperCase());
                },
                isBuffer (obj) {
                    return obj != null && obj.constructor != null &&
                      typeof obj.constructor.isBuffer === 'function' && obj.constructor.isBuffer(obj)
                }, 
                  
                readBuffer(value){
                     return String.fromCharCode.apply( null, new Uint16Array(value))
                } 
}

export {utils as utils};